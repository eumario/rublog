class Setup < Controller
  def index
    @title = "RuBlog Setup"
  end

  def step1
    @title = "Basic Information"
    if request.post?
      flash[:error] = ""
      flash[:error] += "You must provide a name for the blog!<br/>" if request[:name] == ""
      flash[:error] += "You must provide a username!<br/>" if request[:username] == ""
      flash[:error] += "You must provide a password!<br/>" if request[:password] == ""
      flash[:error] += "Passwords do not match!<br/>" if request[:password] != request[:password_confirm]
      flash[:error] += "You must provie an email address!<br/>" if request[:email] == ""
      if flash[:error] == ""
        flash[:error] = nil
        SettingsManager[:site_name] = request[:name]
        SettingsManager[:site_byline] = request[:byline]
        u = User.new(
          :username => request[:username],
          :password => request[:password],
          :admin => true,
          :approved => true,
          :commenter => true,
          :display => request[:display] == "" ? nil : request[:display],
          :email => request[:email])
        u.save
        redirect Setup.r(:step2)
      else
        flash[:error] = flash[:error][0..-6]
      end
    end
  end

  def step2
    @title = "Configuration"
    if request.post?
      if request[:new_reg]
        SettingsManager[:registrations] = true
      else
        SettingsManager[:registrations] = false
      end

      SettingsManager[:reg_approval] = request[:reg_approval].to_sym

      SettingsManager[:commenter] = request[:commenter].to_sym
      SettingsManager[:comment_approval] = request[:comment_approval].to_sym
      SettingsManager[:rublog_setup] = true
      redirect Setup.r(:step3)
    end
  end

  def step3
    @title = "Setup Completed."
  end
end