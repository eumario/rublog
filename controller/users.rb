class Users < Controller
  map '/users'

  before_all do
    if action.method.to_sym != :login and !logged_in?
      flash[:error] = 'You need to be logged in to view that page'
      redirect(Users.r(:login))
    end
  end

  def index
    @users = paginate(User.all)
    @title = 'Users'
  end

  def new
    @user = flash[:form_data]
    @title = 'New User'

    render_view(:form)
  end

  def edit(id)
    @user = flash[:form_data] || User[id]

    if @user.nil?
      flash[:error] = 'The specified user is invalid'
      redirect_referrer
    end

    @title = "Edit #{@user.username}"

    render_view(:form)
  end

  def save
    data = request.subset(:username, :password)
    id   = request.params['id']

    if !id.nil? and !id.empty?
      user = User[id]

      if user.nil?
        flash[:error] = 'The specified user is invalid'
        redirect_referrer
      end

      success = 'The user has been updated'
      error   = 'The user could not be updated'
    else
      user = User.new
      success = 'The user has been created'
      error   = 'The user could not be created'
    end

    begin
      user.update(data)

      flash[:success] = success
      redirect(Users.r(:edit,user.id))
    rescue => e
      Ramaze::Log.error(e)

      flash[:error]         = error
      flash[:form_errors]   = user.errors
      flash[:form_data]     = user

      redirect_referrer
    end
  end

  def delete(id)
    begin
      User.filter(:id => id).destroy
      flash[:success] = 'The specified user has been removed'
    rescue => e
      Ramaze::Log.error(e)
      flash[:error] = 'The specified user could not be removed'
    end

    redirect_referrer
  end

  def login
    if request.post?
      if user_login(request.subset('username','password'))
        flash[:success] = 'You have been logged in'
        redirect(Posts.r(:index))
      else
        flash[:error] = 'You could not be logged in'
      end
    end

    @title = 'Login'
  end

  def logout
    user_logout
    session.clear
    flash[:success] = 'You have been logged out'
    redirect(Users.r(:login))
  end
end