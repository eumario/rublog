class PageEditor < Controller
  def index
    @title = "Page Editor"
    if request.post?
      data = request[:body]
      file = request[:fname]
      File.open(file,"wb") do |fh|
        fh.print(data)
      end
      flash[:success] = "Page has been successfully saved to #{file}"
    end
  end
end