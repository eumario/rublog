# Define a subclass of Ramaze::Controller holding your defaults for all controllers. Note 
# that these changes can be overwritten in sub controllers by simply calling the method 
# but with a different value.

class Controller < Ramaze::Controller
  layout :default
  helper :xhtml
  engine :etanni
  helper :blue_form, :user, :xhtml, :paginate

  trait :paginate => {
    :var => 'page',
    :limit => 10
  }

  trait :user_model => User
  
  def site_name
    sn = SettingsManager[:site_name]
    sn.nil? ? "RuBlog Software" : sn
  end
  
  def site_byline
    sbl = SettingsManager[:site_byline]
    sbl.nil? ? "Simplistic Ruby based Blogging Software" : sbl
  end
end

# Here you can require all your other controllers. Note that if you have multiple
# controllers you might want to do something like the following:
#
#  Dir.glob('controller/*.rb').each do |controller|
#    require(controller)
#  end
#
require __DIR__('posts')
require __DIR__('users')
require __DIR__('admin')
require __DIR__('setup')
require __DIR__('pageeditor')