class Posts < Controller
  map '/'

  provide(:atom, :type => 'application/atom+xml') do |action, body|
    action.layout = false
    body
  end

  before(:edit, :new, :save, :delete) do
    unless logged_in?
      flash[:error] = 'You need to be logged in to view that page'
      redirect(Posts.r(:index))
    end
  end

  def index
    if SettingsManager[:rublog_setup] == false
      redirect Posts.r(:not_setup)
    end
    @posts = paginate(Post.eager(:comments, :user).reverse_order(:created_at))
    @title = 'Posts'
  end

  def feed
    @posts = Post.all

    render_view(:feed)
  end

  def view(id)
    @post = Post[id]

    if @post.nil?
      flash[:error] = 'The specified post is invalid'
      redirect_referrer
    end

    @title        = @post.title
    @created_at   = @post.created_at
    @new_comment  = flash[:form_data] || Comment.new
  end

  def new
    @post   = flash[:form_data]
    @title  = 'New Post'

    render_view(:form)
  end

  def edit(id)
    @post = flash[:form_data] || Post[id]

    if @post.nil?
      flash[:error] = 'The specified post is invalid'
      redirect_referrer
    end

    @title = "Edit #{@post.title}"

    render_view(:form)
  end

  def add_comment
    data    = request.subset(:post_id, :username, :comment)
    comment = Comment.new

    if logged_in?
      data.delete('username')
      data['user_id'] = user.id
    end

    begin
      comment.update(data)
      flash[:success] = 'The comment has been added'
    rescue => e
      Ramaze::Log.error(e)

      flash[:form_errors] = comment.errors
      flash[:error]       = 'The comment could not be added.'
    end
    redirect_referrer
  end

  def save
    data              = request.subset(:title, :body)
    id                = request.params['id']
    data['user_id']   = user.id

    if !id.nil? and !id.empty?
      post = Post[id]

      if post.nil?
        flash[:error] = 'The specified post is invalid'
        redirect_referrer
      end

      success = 'The post has been updated'
      error   = 'The post could not be updated'
    else
      post    = Post.new
      success = 'The post has been created'
      error   = 'The post could not be created'
    end

    begin
      post.update(data)

      flash[:success] = success
      redirect(Posts.r(:edit, post.id))
    rescue => e
      Ramaze::Log.error(e)

      flash[:form_data]   = post
      flash[:form_errors] = post.errors
      flash[:error]       = error

      redirect_referrer
    end
  end

  def delete(id)
    begin
      Post.filter(:id => id).destroy
      flash[:success] = 'The specified post has been removed'
    rescue => e
      Ramaze::Log.error(e.message)
      flash[:error] = 'The specified post could not be removed'
    end

    redirect(Posts.r(:index))
  end

  def not_setup
    @title = "RuBlog Error"
  end
end