class Category < Sequel::Model
  one_to_many :posts
  def validate
    validates_presence(:name)
  end
end