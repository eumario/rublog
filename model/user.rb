class User < Sequel::Model
  one_to_many :posts
  one_to_many :comments

  def self.authenticate(creds)
    username, password = creds['username'], creds['password']

    if username.nil? or username.empty?
      return false
    end

    user = self[:username => username]

    if !user.nil? and user.password == password
      return user
    else
      return false
    end
  end

  def password=(password)
    if password.nil? or password.empty?
      return
    end

    password = BCrypt::Password.create(password,:cost => 10)

    super(password)
  end

  def password
    password = super

    if !password.nil?
      return BCrypt::Password.new(password)
    else
      return nil
    end
  end

  def is_admin?
    admin
  end

  def is_commenter?
    commenter
  end

  def display_name
    self[:display].nil? ? self[:username] : self[:display] == "" ? self[:username] : self[:display]
  end

  def validate
    validates_presence(:username)
    validates_presence(:password) if new?
  end
end