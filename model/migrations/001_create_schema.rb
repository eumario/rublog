Sequel.migration do
  up do
    STDOUT.print "Creating table users..."
    create_table(:users) do
      primary_key :id

      # Login Information
      String :username, :null => false
      String :password, :null => false

      # Flags
      TrueClass :admin, :null => false, :default => false
      TrueClass :commenter, :null => false, :default => true
      TrueClass :approved, :null => false, :default => false

      # Personal Information
      String :display, :null => true
      String :email, :null => true
      String :website, :null => true
      String :aim, :null => true
      String :ymsg, :null => true
      String :msn, :null => true
      String :skype, :null => true
      String :about, :null => true
    end
    STDOUT.puts " Done."

    STDOUT.print "Creating table categories..."
    create_table(:categories) do
      primary_key :id

      String :name, :null => false
      foreign_key :category_id, :categories, :null => true, :on_update => :cascade,
        :on_delete => :cascade, :key => :id
    end
    STDOUT.puts " Done."
    
    STDOUT.print "Creating table posts..."
    create_table(:posts) do
      primary_key :id

      String :title, :null => false
      File :body, :null => false

      Time :created_at
      Time :updated_at

      foreign_key :category_id, :categories, :on_update => :cascade,
        :on_delete => :cascade, :key => :id

      foreign_key :user_id, :users, :on_update => :cascade,
        :on_delete => :cascade, :key => :id
    end
    STDOUT.puts " Done."
    
    STDOUT.print "Creating table pages..."
    create_table(:pages) do
      primary_key :id

      String :title, :null => false
      File :body, :null => false

      Time :created_at
      Time :updated_at

      foreign_key :user_id, :users, :on_update => :cascade,
        :on_delete => :cascade, :key => :id
    end
    STDOUT.puts " Done."

    STDOUT.print "Creating table menus..."
    create_table(:menus) do
      primary_key :id

      String :name, :null => false
      foreign_key :menu_id, :menus, :null => true, :on_update => :cascade,
        :on_delete => :cascade, :key => :id
      foreign_key :page_id, :pages, :null => true, :on_update => :cascade,
        :on_delete => :cascade, :key => :id
    end
    STDOUT.puts " Done."
    
    STDOUT.print "Creating table comments..."
    create_table(:comments) do
      primary_key :id

      String :username
      String :body, :null => false, :text => true

      Time :created_at

      foreign_key :post_id, :posts, :on_update => :cascade,
        :on_delete => :cascade, :key => :id

      foreign_key :user_id, :users, :on_update => :cascade,
        :on_delete => :cascade, :key => :id

      TrueClass :approved, :default => false
    end
    STDOUT.puts " Done."
    
    STDOUT.print "Creating table settings..."
    create_table(:settings) do
      String :key, :primary_key => true
      File :value
    end
    STDOUT.puts " Done."

    STDOUT.print "Initializing settings (rublog_setup = false)..."
    self[:settings].insert([:key,:value],["rublog_setup",false.to_yaml])
    STDOUT.puts " Done."
  end

  down do
    [:settings,:comments,:posts,:menus,:pages,:users,:categories].each do |table|
      STDOUT.print "Dropping #{table}..."
      begin
        drop_table(table)
      rescue => e
        STDOUT.puts " Failed to drop table #{table}\nReason: #{e.inspect}:#{e.message}"
        raise e
      end
      STDOUT.puts " Done."
    end
  end
end