class Comment < Sequel::Model
  plugin :timestamps, :create => :created_at, :update => :updated_at

  many_to_one :post
  many_to_one :user

  def validate
    validates_presence(:comment)

    unless self.user_id
      validates_presence(:username)
    end
  end

  def username
    if user and user.username
      return user.username
    else
      return super
    end
  end
end