class Post < Sequel::Model
  plugin :timestamps, :create => :created_at, :update => :updated_at

  many_to_one :user
  one_to_one :category
  one_to_many :comments

  def validate
    validates_presence([:title, :body])
  end
end