class SettingsManager
  def self.[](key)
    sett = Setting[:key => key.to_s]
    sett.nil? ? nil : sett.value
  end

  def self.[]=(key,val)
    sett = Setting[:key => key.to_s]
    if sett.nil?
      sett = Setting.new
      sett.key = key
    end
    sett.value = val
    sett.save
  end
end