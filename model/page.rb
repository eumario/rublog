class Page < Sequel::Model
  plugin :timestamps, :create => :created_at, :update => :updated_at

  many_to_one :user

  def validate
    validates_presence([:title, :body])
  end
end