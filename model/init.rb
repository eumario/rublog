DB = Sequel.connect(RuBlog::Config::Database[RuBlog::Config::Environment])

Sequel::Model.plugin(:validation_helpers)

Sequel.extension(:migration)

DB.extension(:pagination)

require __DIR__('comment')
require __DIR__('post')
require __DIR__('user')
require __DIR__('page')
require __DIR__('category')
require __DIR__('menu')
require __DIR__('setting')
require __DIR__('settings_manager')