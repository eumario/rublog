class Menu < Sequel::Model
  one_to_many :submenu, :key => :menu_id, :class => :Menu
  one_to_one :parent, :key => :menu_id, :class => :Menu

  def validate
    validates_presence(:name)
  end
end