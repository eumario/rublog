class Setting < Sequel::Model
  
  def validate
    validates_presence([:key,:value])
  end

  def value
    if @value.nil?
      @value = Psych::load(values[:value])
    end
    return @value
  end

  def value=(val)
    @value = val
    values[:value] = val.to_yaml
  end
end

Setting.unrestrict_primary_key