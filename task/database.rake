def db_uri
  dbinfo = RuBlog::Config::Database[RuBlog::Config::Environment]
  uri = "#{dbinfo["adapter"]}://"
  if dbinfo["adapter"] == "sqlite"
    uri += dbinfo["database"]
  else
    uri += dbinfo["username"] + "@" + dbinfo["host"]
    uri += ":" + dbinfo["port"] unless dbinfo["port"].nil?
    uri += "/" + dbinfo["database"]
  end
  uri
end

namespace :db do
  app = File.expand_path('../../app', __FILE__)
  require 'sequel'
  require 'psych'
  
  namespace :migrate do
    Sequel.extension :migration
    require app
    
    task :connect do
      $DB = Sequel.connect(RuBlog::Config::Database[RuBlog::Config::Environment])
    end
    
    desc 'Perform an upgrade to the database'
    task :up => :connect do
      puts "Performing Upgrade on Database #{db_uri}..."
      Sequel::Migrator.run($DB,'model/migrations')
      puts "*** db:migrate:up executed ***"
    end
    
    desc 'Perform a upgrade/downgrade to a specific revision. !!!WARNING!!! Can truncate data.'
    task :to, [:version] => :connect do |cmd,args|
      vers = args[:version].to_i
      puts "Performing Revision migration to #{vers} on database #{db_uri}..."
      Sequel::Migrator.run($DB,'model/migrations', :target => vers)
      puts "*** db:migrate:to:#{vers} executed ***"
    end
    
    desc 'Downgrade back to 0 for the database.  !!!WARNING!!! Will erase all data'
    task :down => :connect do
      puts "Downgrading back to Revision 0 on database #{db_uri}..."
      Sequel::Migrator.run($DB,'model/migrations', :target => 0)
      puts "*** db:migrate:down executed ***"
    end
    
    desc 'Destroy Database, and Start a new one, up to the latest migrations.'
    task :restart do
      puts "Restarting database #{db_uri}..."
      Rake::Task["db:migrate:connect"].execute
      Rake::Task["db:migrate:down"].execute
      Rake::Task["db:migrate:up"].execute
      puts "*** db:migrate:restart executed ***"
    end
  end

  desc 'Sets up the database with default information.'
  task :install do
    require app
    User.create(
      :username => 'admin',
      :password => 'admin',
      :admin => true,
      :approved => true,
      :display => 'Administrator')

    Post.create(
      :title => 'Example Post',
      :body => 'This is a post that uses Markdown!',
      :user_id => User[:username => 'admin'].id
    )
    puts "*** db:install executed ***"
  end
end
