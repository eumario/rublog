module RuBlog
  class Config
    TEMPLATE_CLASS = Class.new do
      def self.init(vals)
        @vals = vals
      end

      def self.[](var)
        @vals[var]
      end

      def self.[]=(var,val)
        @vals[var] = val
      end
    end
    
    def self.load
      data = Psych::load_file(__DIR__("../config/application.yml"))
      data.each do |k,v|
        RuBlog::Config.const_set(k.to_s.camel_case,TEMPLATE_CLASS)
        RuBlog::Config.const_get(k.to_s.camel_case).init(v)
      end
    end

    Environment = ENV["RUBLOG_ENV"].nil? ? ENV["RACK_ENV"].nil? ? "development" : ENV["RACK_ENV"] : ENV["RUBLOG_ENV"]
  end
end