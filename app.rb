# This file contains your application, it requires dependencies and necessary
# parts of the application.
require 'rubygems'
require 'ramaze'
require 'sequel'
require 'sqlite3'
require 'bcrypt'
require 'rdiscount'
require 'miso'
require 'psych'

# Make sure that Ramaze knows where you are
Ramaze.options.roots = [__DIR__]

# Custom written libraries
require __DIR__('lib/init')

RuBlog::Config.load

require __DIR__('model/init')
require __DIR__('controller/init')